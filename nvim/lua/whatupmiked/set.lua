-- Setting configuration for nvim   
vim.g.mapleader = "\\"

-- LSP?
-- Stolen from: https://github.com/BrotifyPacha/nvim/blob/b12fe7e5b52dcee3b3d3e78d031cb01d862a3785/lua/lsp/init.lua#L28-L29
-- help vim.lsp.buf.hover()
-- help vim.lsp.buf.rename
local opts = { noremap = true, silent = true }
vim.api.nvim_set_keymap("n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
vim.api.nvim_set_keymap("n", "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", opts)
vim.api.nvim_set_keymap("n", "K", "<cmd>lua vim.lsp.buf.hover()<CR>", opts)
vim.api.nvim_set_keymap("n", "<C-k>", "<cmd>lua vim.lsp.buf.code_action()<CR>", opts)
vim.api.nvim_set_keymap("v", "<C-k>", "<cmd>lua vim.lsp.buf.range_code_action()<CR>", opts)
vim.api.nvim_set_keymap("n", "gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts)
vim.api.nvim_set_keymap("n", "gr", "<cmd>lua vim.lsp.buf.references()<CR>", opts)
-- vim.api.nvim_set_keymap("n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", { noremap = true, silent = true })
-- vim.api.nvim_set_keymap("n", "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", { noremap = true, silent = true })

-- Colors
vim.opt.termguicolors = true

-- Numbering
vim.opt.number = true -- Turn on line numbering

-- Indentation
vim.opt.autoindent = true -- Indent when moving to next line while writing code.
vim.opt.smartindent = true
vim.opt.tabstop = 4 -- # of visual spaces per TAB when displaying file.
vim.opt.softtabstop = 4 -- # of spaces in TAB when editing a file.
vim.opt.shiftwidth = 4 -- When using << or >> # of spaces to indent
vim.opt.expandtab = true -- Do not use \t convert to spaces

-- VIM File management   
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

-- MISC
vim.opt.guicursor = ""
vim.opt.scrolloff = 8
vim.opt.updatetime = 50
vim.opt.colorcolumn = "80" -- Add a vertical column at a location
vim.opt.cursorline = true -- show visual line on the current line your cursor is on
vim.opt.ruler = true -- show cursor location
vim.opt.showmatch = true -- show the matching part of the pair for [] {} ()
vim.opt.list = true
vim.opt.listchars:append({trail = '⋅'})
vim.opt.listchars:append({nbsp = '⋅'})
--vim.opt.listchars:append({tab = '▷'})
--vim.opt.listchars = { tab='▷', trail='⋅', nbsp='⋅' } --
--vim.opt.listchars = { tab = '▷', trail = '⋅', nbsp = '⋅' }
--vim.o.listchars = 'tab:▷,trail:⋅,nbsp:⋅'

-- set backspace=indent,eol,start " make backspace work in insert mode
-- set list listchars=tab:▷\ \,trail:⋅,nbsp:⋅ " Add some specail characters for spaces and tabs
-- "set list listchars=tab:\ \ ,trail:⋅,nbsp:⋅ " Add some specail characters for spaces and tabs
-- set shell=/bin/bash " Set the shell to bash for command execution


-- Searching
vim.opt.incsearch = true -- Search as characters are entered
vim.opt.hlsearch = true -- Highlight search matches
vim.opt.ignorecase = true -- Perform case-insensitive search
vim.opt.smartcase = true  -- ... but not when search pattern contains uppercase

-- Windowing
vim.opt.splitright = true -- Split vertical windows right of the current window
vim.opt.splitbelow = true -- Split horizontal windows below the current window

-- Obselete for nvim?
-- set signcolumn=number
-- set completeopt+=popup
-- set completepopup=align:menu,border:off,highlight:Pmenu
-- set shortmess+=c -- hide omnicompletion in status bar?
-- set keywordprg=:Man -- Support for hitting K to open manpage over selection
-- set wildmenu " enable visual autocomplete for command menu -- defaults on
-- set showcmd " show command in bottom bar -- defaults on
-- set encoding=utf-8 " set default encoding to utf-8 -- defaults utf-8
-- set showmode " show mode in bottom bar -- defaults on
-- set history=50 " Set the command history to 50
-- set autoread " Automatically reread changed files without asking anything
