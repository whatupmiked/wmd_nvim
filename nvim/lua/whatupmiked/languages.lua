-- In this file I want to establish language specific settings (not related to LSP)

-- Python
-- au BufNewFile *.py 0r ~/.vim/skel/skel.py
vim.api.nvim_create_autocmd('BufNewFile',{
    pattern = '*.py',
    command = '0r ~/.config/nvim/skel/skel.py'
})
-- golang
-- au BufNewFile *.go 0r ~/.vim/skel/skel.go
vim.api.nvim_create_autocmd('BufNewFile',{
    pattern = '*.go',
    command = '0r ~/.config/nvim/skel/skel.py'
})
-- RestructuredText
-- au BufNewFile *.rst 0r ~/.vim/skel/skel.rst
vim.api.nvim_create_autocmd('BufNewFile',{
    pattern = '*.rst',
    command = '0r ~/.config/nvim/skel/skel.rst'
})
--
-- YAML (Yet Another Markup Language)
-- au BufNewFile *.yaml,*.yml 0r ~/.vim/skel/skel.yaml
vim.api.nvim_create_autocmd('BufNewFile',{
    pattern = {'*.yaml', '*.yml'},
    command = '0r ~/.config/nvim/skel/skel.yaml'
})
-- Michael Man Pages
-- au BufNewFile *.msd 0r ~/.vim/skel/skel.msd
vim.api.nvim_create_autocmd('BufNewFile',{
    pattern = {'*.msd'},
    command = '0r ~/.config/nvim/skel/skel.msd'
})
-- Shell Scripting (:Man bash)
-- au BufNewFile *.sh 0r ~/.vim/skel/skel.sh
vim.api.nvim_create_autocmd('BufNewFile',{
    pattern = {'*.sh'},
    command = '0r ~/.config/nvim/skel/skel.sh'
})
-- XML Manpages
-- au BufNewFile *.manxml 0r ~/.vim/skel/skel.manxml setlocal filetype=xml
vim.api.nvim_create_autocmd('BufNewFile',{
    pattern = {'*.manxml'},
    command = '0r ~/.config/nvim/skel/skel.manxml'
})
-- tmux.toml 
vim.api.nvim_create_autocmd('BufNewFile',{
    pattern = '*tmux.toml',
    command = '0r ~/.config/nvim/skel/skel-tmux.toml'
})
-- LUA files
vim.api.nvim_create_autocmd('BufNewFile',{
    pattern = '*lua',
    command = '0r ~/.config/nvim/skel/skel.lua'
})

-- Learn X in Y LUA
vim.api.nvim_create_autocmd('BufNewFile',{
    pattern = '*lua.learn',
    command = '0r ~/.config/nvim/skel/skel.lua.learn'
})
