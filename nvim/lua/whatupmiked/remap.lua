vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)
vim.keymap.set("n", "<CR>", "<cmd>AerialToggle! right<cr>", { desc="Toggle the tagbar on the right" })
vim.keymap.set("n", "<leader>qc", "<cmd>cclose<cr>", { desc="Close quickfix" })
vim.keymap.set("n", "<leader>qo", "<cmd>copen<cr>", { desc="Open quickfix" })
-- noremap <Up> <Nop>
-- noremap <Down> <Nop>
-- noremap <Left> <Nop>
-- noremap <Right> <Nop>
