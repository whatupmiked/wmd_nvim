require("whatupmiked.remap")
require("whatupmiked.set")
-- Importing lua files remap.lua and set.lua from the whatupmiked directory
require("whatupmiked.languages")
-- Importing language specifc settings like aucommands
