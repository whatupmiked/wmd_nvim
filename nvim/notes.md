# TODO

[ ] Syntax highlighting for robot files (or txt)
[ ] Syntax highlighting for code in markdown?
[ ] Change color of tab and spaces at end of line

# Pre-requisites

Install Packer:
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim

Navigate to packer.lua and `:so` source it then `:PackerSync`

Need npm to install some plugins (dnf install nodejs-npm)
Need to install c++ for treesitter (dnf install gcc gcc-c++ make)

# Notes
LSP search = :Mason
Installing Plugins = :so, :PackerSync
To enter terminal in a split: :split termin://bash
To exit terminal Ctrl+\ Ctrl+n

Use :TSUpdate <language> if you get treesitter errors

I need skeletons for file-types

I used to use Ctrl-n for nerdtree maybe I should use this for telescope file navigation
now

:Man just works I think or telescope does it?

To toggle off linting comments do :LspStop use :LspStart to enable

## Useful doc pages

:help lua-guide
:help lua-concepts
:help luaref

# Plugins

I am using packer for plugin management.
I am using  'mason' for LSP stuff.
I have 'telescope' for fuzzyfinding. (help Telescope)
I also have 'harpoon'.
I also have treesitter.
I also have:
 vim-fugitive
 vim-commentary

aerial 

 In VIM I have:
  tagbar
  vim-flake8
  jedi-vim
  black
  robotframework-vim
  govim
  fzf
  vim-airline
  vim-snipmate
  vim-snippets
  vim-css-color
  vim-surround
  nerdtree

Make snapshots to rollback bad plugin updates: https://dev.to/vonheikemen/packernvim-how-to-recover-from-a-bad-plugin-update-2813

## Treesitter

[https://tree-sitter.github.io/tree-sitter/](treesitter)
help treesitter
help nvim-treesitter-quickstart

## Telescope

[https://github.com/nvim-telescope/telescope.nvim](telescope)
[https://github.com/nvim-telescope/telescope.nvim/wiki/Showcase](telescope showcase)
help telescope

:Telescope keymaps
:Telescope live_grep
:Telescope find_files
:Telescope buffers

keymaps in nvim/after/plugin/telescope.lua

## Harpoon

```python
def bob(var: str) -> None:
    print("something")
    pass
```

[https://github.com/ThePrimeagen/harpoon](harpoon)

keymaps in nvim/after/plugin/harpoon.lua

Harpoon stores files for jumping too (apparently per project). It is for jumping around between files.

It also has a mode for managing tmux windows and sending commands too them which could be very useful for my workflow.

## LSP

## Aerial

zc, zo, zf to collapse sections.

## Lualine

[https://github.com/nvim-lualine/lualine.nvim](lualine)

I installed hack font for the icons
https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/Hack.zip

I used https://docs.fedoraproject.org/en-US/quick-docs/fonts/ to install on fedora but you need to set your terminal emulator to use it. So this would be alacritty on Fedora or on MacOS I installed it for iterm2.

# Keymaps

The keymaps come primarily from whatupmiked/set.lua

The leader key is '//'

New bindings are mapped in whatupmiked/remap.lua

The structure is:
    vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)
    (mode, mapping, action)

    \\pv for explorer

## LSP Keymaps

:LspInfo

:help lsp-zero-keybindings

When in tab-completion:
 - ctrl-f to scroll down and ctrl-u to scroll up
 - ctrl-p for next, ctrl-n for previous
 - ctrl-c to cancel


# Colorschemes

:colorscheme <tab>
nvim/after/plugin/colors.lua

# Completion

File Completion: ctrl-x ctrl-f

# Skeleton Files
I have stored the templates in ./skel/ and loaded them via ./lua/whatupmiked/languages.lua


I should update my python template to include pytest and some basics like file management, environment variables, logging, generic serializer (json, yaml, csv). [X]
I should update my shell template with logger. [x]
I should add skeletons for xml, xsd, xslt, ts (?) while it is fresh in my head.
I should add a template for LaTex
I should make a template for notes.md.
I should make a template for HTML, CSS.
I should make a template for *tmux.toml.
```vim

"""""" SYNTAX
au BufNewFile *.go 0r ~/.vim/skel/skel.go
au BufNewFile *.rst 0r ~/.vim/skel/skel.rst
au BufNewFile *.yaml,*.yml 0r ~/.vim/skel/skel.yaml
au BufNewFile *.msd 0r ~/.vim/skel/skel.msd
au BufNewFile *.sh 0r ~/.vim/skel/skel.sh
au BufNewFile *.manxml 0r ~/.vim/skel/skel.manxml setlocal filetype=xml
au BufNewFile *.py 0r ~/.vim/skel/skel.py
```

```vim
au BufNewFile,BufRead *.json,*.md,*.yml,*.yaml setlocal expandtab ts=2 sw=2 sts=2 foldmethod=indent foldlevel=2 " Yaml, Markdown, JSON Syntax settings
au BufNewFile,BufRead *.js,*.html,*.css,*.tmpl set expandtab ts=2 sts=2 sw=2 number relativenumber
au BufNewFile,BufRead *.sh setlocal ts=2 sts=2 sw=2 textwidth=80
au BufNewFile,BufRead *.msd setlocal filetype=groff " MSD man-page settings
"au BufNewFile,BufRead *.py setlocal foldmethod=indent foldlevel=2 ts=4 sts=4 sw=4 expandtab smartindent textwidth=80 fileformat=unix number
au BufNewFile,BufRead *.go setlocal noet ts=4 sw=4 sts=4 number foldmethod=indent foldlevel=2 " Go Syntax settings
```

# Languages

 - Python
   - Path:
   - How To:
     - Toggle on/off linting
 - Shell
 - LaTex
 - CLang
 - Typescript
 - Robotframework?
 - Ansible?
 - XML/XSLT/XSD?
 - HTML / CSS
 - golang

## Latex Setup

vimtext seems to be the defactor for latex editing with vim.
Ref1: 
Ref2: 

Needs latexmk for compiling latex and use zathura for PDF viewer.

VimtexCompile
VimtexClean
VimtexView

Use texlab (rust) for Lsp
