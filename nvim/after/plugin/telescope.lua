local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>f', builtin.find_files, {}) -- :help telescope.builtin.find_files
vim.keymap.set('n', '<leader>g', builtin.live_grep, {}) -- :help telescope.builtin.live_grep
vim.keymap.set('n', '<leader>b', builtin.buffers, {}) -- :help telescope.builtin.buffers
vim.keymap.set('n', '<leader>tr', builtin.registers, {}) -- :help telescope.builtin.registers
vim.keymap.set('n', '<leader>tj', builtin.jumplist, {}) -- :help telescope.builtin.jumplist
-- vim.keymap.set('n', '<leader>fs', function()
-- 	builtin.grep_string({ search = vim.fn.input("Grep > ") });
-- end)
