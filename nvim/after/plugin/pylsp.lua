-- https://github.com/palantir/python-language-server/issues/880
local function which_python()
    local f = io.popen('env which python', 'r') or error("Fail to execute 'env which python'")
    local s = f:read('*a') or error("Fail to read from io.popen result")
    f:close()
    return string.gsub(s, '%s+$', '')
end

require'lspconfig'.pylsp.setup{
    settings = {
        pylsp = {
            plugins = {
                pycodestyle = {
                    ignore = {'E501'},
                    maxLinLength = 120
                    }
                },
                isort = {
                    enabled = true,
                    profile = "black",
                },
                jedi = {
                    environment = which_python(),
                }
            }
        }
    }
