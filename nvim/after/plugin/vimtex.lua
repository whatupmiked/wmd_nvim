-- Configuration for the vimtex plugin
vim.g['vimtex_view_method'] = 'zathura'

-- latexmk is the back-end
-- vim.g['vimtex_compiler_method'] = 'latexrun'
--
-- Do not open quickfix window on errors
-- vim.g['vimtex_quickfix_mode'] = 0
-- Ignore mappings
-- vim.g['vimtex_mappings_enabled'] = 0
-- Auto Indent
-- vim.g['vimtex_indent_enabled'] = 0
-- Disable vimtex syntax highlighting
-- vim.g['vimtex_syntax_enabled'] = 0

-- localleader for key mappings
-- let maplocalleader = ','
