-- https://github.com/robocorp/robotframework-lsp/blob/master/robotframework-ls/docs/config.md
require'lspconfig'.robotframework_ls.setup{
    settings = {
        robot = {
            pythonpath = {
                -- "/opt/venv/robot6_py312/lib/python3.12/site-packages/",
               "/home/michael/projects/work/qa/repos/auto/",
               "/home/michael/projects/work/qa/repos/",
               -- "/home/michael/.local/lib/python3.11/site-packages/",
               -- "/usr/lib/python3.11/site-packages/"
           },
           variables = {
               VF_DIR = "/home/michael/projects/work/qa/repos/auto"
           }
        }
        }
    }
