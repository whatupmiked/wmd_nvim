-- https://github.com/ThePrimeagen/harpoon
--
-- The philsophy of the file harpoon is that within a project or feature there are
-- only a few files you need to work with so those files can be stored on a quicknav
-- list PER-PROJECT and then navigated to. That is what the nav_file functions 
-- and hotkeys are about.
--
-- I need to understand how the PER-PROJECT aspect of this is working.
local mark = require("harpoon.mark")
local ui = require("harpoon.ui")

--
-- The command harpoon seems very useful for sending commands like build scripts
-- or linters to tmux panes related to your project. (i.e. the CODE pane)
-- I need to figure out how to set these up per project and also to execute the
-- command stored in the pane I care about.
--
-- For example a good use-case for this would be the infra/kvm projects setting
-- up some linting commands and some dryrun commands for ansible and also perhaps
-- storing the ansible-playbook commands.
--
-- Another example would be robot dryrun.
local cmd_ui = require("harpoon.cmd-ui")
local tmux = require("harpoon.tmux")

require("harpoon").setup({
    menu = {
        width = vim.api.nvim_win_get_width(0) - 4;
    }
})

vim.keymap.set("n", "<leader>a", mark.add_file)
vim.keymap.set("n", "<leader>h", ui.toggle_quick_menu) -- This is the jump menu
vim.keymap.set("n", "<leader>c", cmd_ui.toggle_quick_menu) -- This is the jump menu
vim.keymap.set("n", "<leader>t", function() tmux.sendCommand("{down-of}", "pwd\r") tmux.sendCommand("{down-of}", "fd bash\r") end) -- Harpoon commands

-- These are the quicknav files, remember the philsophy here is that there are
-- only a few files that you need in a project/feature.
vim.keymap.set("n", "<leader>1", function() ui.nav_file(1) end)
vim.keymap.set("n", "<leader>2", function() ui.nav_file(2) end)
vim.keymap.set("n", "<leader>3", function() ui.nav_file(3) end)
vim.keymap.set("n", "<leader>4", function() ui.nav_file(4) end)
