.\"
.\" Man page for apropos
.\"
.\" Copyright (c) 2020, Michael S. Doyle
.\"
.\" You may distribute under the terms of the GNU General Public
.\" License as specified in the README file that comes with the man 1.0
.\" distribution.
.\"
.\"
.TH cmd-name 1 DATE
.LO 1
.SH NAME
apropos \- search the whatis database for strings
.SH SYNOPSIS
.B cmd-name
.RB [\| \-flags \|]
\&.\|.\|.
.SH DESCRIPTION
Each manual page has a short description available within it.
.B cmd-name
does some thing
.IR well .
.SH OPTIONS
.TP
.BR \-h ", " \-\-help
Print the help for something
.TP
.BR \-a ", " \-\-another
Another option
.TP
.SH "EXIT STATUS"
.TP
.B 0
Successful program execution.
.TP
.B 1
An error has occured during execution.
.SH SYSTEM
.TP
.B ENV
If
.RB $ ENV
is set something happens because of this environment variable.
.SH FILES
.TP
.I ~/.vim is an example of a folder
.SH AUTHOR
Michael S. Doyle
.SH "SEE ALSO"
anotherpage(1), evenanother(1).
