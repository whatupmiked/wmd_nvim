#!/bin/env bash
#set -euo pipefail
IFS=$'\n\t'

[ -z ${DEBUG+x} ] || set -x # Set debugging if ${DEBUG} is set

# Unix shell script tactics: https://github.com/SixArm/unix-shell-script-tactics/tree/main
# Bash Bible: https://github.com/dylanaraps/pure-bash-bible
# Google Style Guide: https://google.github.io/styleguide/shellguide.html
#
### Detecting the directory of a script ###
# https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script
#
# SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

### TRACING ###
# set -o xtrace
# PS4='+\t (cmd) '
# # STUFF
# set +0 xtrace
# PS4='\t '
#
### TIMING ###
# start=$SECONDS
# # STUFF
# let sec=$(($SECONDS - $start))
# let min=$(( sec / 60 ))
# let min_sec=$(( sec % 60 ))
# echo "$min minutes and $min_sec seconds"

cleanup() { :; }

# Setup loggers
if [ -t 1 ] && [ -z "${NO_COLOR:-}" ] && [ "${TERM:-}" != "dumb" ]; then
    GREEN="[0;32m"; RED="[0;31m"; YELLOW="[0;33m";PURPLE="[0;35m"; NC="[0m"
    die() { cleanup; echo -e "\n[$(date +'%Y-%m-%dT%H:%M:%S%z')]: ${RED}${@}${NC}\n" >&2; exit; }
    warn() { echo -e "\n${YELLOW}${@}${NC}\n"; }
    step() { echo -e "${GREEN}${@}${NC}\n"; }
else
    die() { cleanup; echo -e "\n[$(date +'%Y-%m-%dT%H:%M:%S%z')]: ${@}\n" >&2; exit; }
    warn() { echo -e "\n${@}\n"; }
    step() { echo -e "\n${@}"; }
fi

# When succeed do step.
# ls ~/.bashrc && step "Checking for presence of .bashrc"

# When fail do warn
# ls ~/.nothing || die "Checking for ~/.nothing which does not exist"

make_boilerplate() {
  step "Adding boilerplate for ${1}"
  cat <<__EOF | sed 's/  //' > ${1}
  # boiler
  ---
__EOF
}

make_boilerplate_sudo() {
  step "Adding boilerplate for ${1}"
  cat <<__EOF | sed 's/  //' | sudo tee ${1}
  # boiler
  ---
__EOF
}

bob() {
  if [[ "$1" =~ -n|--name ]]; then
    shift
    name="$1"
  fi
  echo "${name}"
}

favorites() {
  while [ $# -gt 1 ]; do
    case "$1" in
      -n|--name) shift; name="$1";;
      -f|--food) shift; food="$1";;
      -c|--color) shift; color="$1";;
      -s|--season) shift; season="$1";;
      -p|--pet) shift; pet="$1";;
    esac
    shift
  done
  cat << EOF
${name} favorite food: ${food}
${name} favorite color: ${color}
${name} favorite season: ${season}
${name} favorite pet: ${pet}
EOF
}

json() {
  cat << EOF
{
  "var1": "bla",
  "var2": "bla2",
  "var3": "bla3"
}
EOF
}

usage() {
    printf 'How to use this script\n'
    exit
}

main() {
  while [ $# -gt 1 ]; do
    case "$1" in
      -a|--alpha)shift; name="$1";;
      -b|--beta)shift; food="$1";;
      -g|--gamma)shift; color="$1";;
      *) usage ;;
    esac
  done
  step Running favourites
  favorites --name bob -f pizza -c blue --season summer -p dog
  step Checking for jq
  ! which jq 2> /dev/null && echo "need jq." 1>&2 && return 2
  result=$(json)
  name=$(jq -r '.var1' <<< "${result}")
  home=$(jq -r '.var2' <<< "${result}")
  city=$(jq -r '.var3' <<< "${result}")
  die Terminating with an error
}

main $@

