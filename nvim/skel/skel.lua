#!/bin/env lua

-- Reference:  https://www.lua.org/pil/21.1.html
-- Multi-line strings using [[
TXT = [[this is a multi-line
string which spans multiple lines]]
-- The base data structure in LUA is a TABLE
PARSED = {}

io.input("file")
TXT = io.read("*all")

-- This basic implementation using gmatch skips empty lines (i.e. "\n\n")
for line in TXT:gmatch("[^\r\n]+") do
    table.insert(PARSED, line)
end

-- Create an enumeration of a value-based table (i.e. list)
for i, val in ipairs(PARSED) do
    print(i, val)
end

-- Open *.lua.learn for LEARN X in Y LUA
