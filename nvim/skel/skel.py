#!/usr/bin/env python3
"""Module documentation.

Further details on the module.
"""

# Imports
import argparse
import csv
import logging
import json
import os
import re
import requests
import sys
from datetime import datetime
from pathlib import Path
# from rich import print
from rich.console import Console
from rich.table import Table
from rich.logging import RichHandler

import pytest

# Global variables
DEBUG = False


# Class declarations
class CustomException(Exception):
    """Generic exception class."""

    pass

# getattr, setattr


# Function declarations
# Logging
def info(msg: str) -> None:
    logging.info(msg)


def debug(msg: str) -> None:
    if DEBUG:
        logging.debug(msg)


# Environment Variables
def debug_env() -> None:
    var = "HOME"
    info(f"{var} = {os.getenv(var)}")


def set_env(var: str, value: str) -> None:
    """Set an environment variable value."""
    os.environ[var] = value


# Dates and times
def convert_datetime(date: datetime) -> str:
    return datetime.strptime(date, "%a %b %d %H:%M:%S %Y")


# File format serialization
def parse_csv(file: Path) -> list:
    """Convert the CSV file to a list."""
    csv_list = []
    with file.open() as f:
        for row in csv.reader(f):
            if "something" in row:
                continue  # skip
            else:
                csv_list.append(row)
    return csv_list


def parse_json(file: Path) -> dict:
    with file.open() as f:
        return json.loads(f.read())


def write_json(var: dict, file: Path) -> None:
    j = json.dumps(var)
    # Write to File
    with file.open() as f:
        f.write(j)

    # Write to URL
    headers = {"accept": "application/json", "Content-Type": "application/json"}
    url = "http://example.net"
    try:
        r = requests.post(url, headers=headers, data=j)
    except Exception as e:
        debug(f"Request to {url} returned {r.status_code} {r.reason}\n{e}")


# Output formatting
def make_table() -> None:
    """Make a table with rich."""
    table = Table(caption="The caption of the table", caption_style="bold plum1")
    table.add_column("String", justify="left", style="cyan")
    table.add_column("Int", justify="right", style="green")
    li = ["a", "b", "c", "d", "e"]
    for i, v in enumerate(li):
        table.add_row(v, i)
    console = Console()
    console.print(table)


# Regular Expressions
def regex() -> None:
    one = re.compile(r'[a-zA-Z0-9 -]+')  # Letters and numbers and space and dash
    one.sub("replacement", "original_string")  # Replace original_string using compiled regex


# Define Tests
idx = ((0, "0"),
       pytest.param((1, "1"), marks=pytest.mark.skip),
       pytest.param((2, "2"), marks=pytest.mark.xfail),
       pytest.param((3, "3"), marks=[pytest.mark.skip, pytest.mark.xpass]),
       )
ids = ["testcase_1", "testcase_2", "testcase_3", "testcase_4"]
FUNCS = pytest.mark.parametrize(("id_i", "id_s"), idx, ids=ids)


def f(i: str) -> int:
    return int(i)


@FUNCS
def test_f(id_i, id_s):
    assert f(id_i) == id_s


def main():
    parser = argparse.ArgumentParser(description="A generic argument parser",
                                     epilog="Use it like: ./xyz --flag")
    parser.add_argument("-d", "--debug", dest="debug", action="store_true", default=False, help="Enable debug logging")
    parser.add_argument("-v", "--variable", dest="var", default="a", choices=["a", "b", "c"], help="Define a variable")
    parser.add_argument("-f", "--file", dest="file", help="Define a file to read")
    parser.add_argument("-n", "--nothing", dest="nothing", default=False, action="store_true")
    args = parser.parse_args()

    # Exit with error code
    if args.nothing:
        sys.exit(1)

    # Change a global variable
    if args.debug:
        global DEBUG
        DEBUG = True

    # Setup Debug Loggin
    log_level = logging.DEBUG if DEBUG else logging.INFO
    logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s", level=log_level, handlers=[RichHandler(markup=True)])


# Main body
if __name__ == '__main__':
    main()
