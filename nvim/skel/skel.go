/* Package michael is how you start a go program to document a packet
   called michael. The first sentence of the package comments will appear
   in godoc's packagelist.

   For tips on go program structure: https://golang.org/doc/effective_go.html

*/
package michael

import (
	l "log"
	"os"
	"fmt"
	"runtime"
)

var (
	LogWarning *l.Logger
	LogInfo    *l.Logger
	LogError   *l.Logger
	LogDebug   *l.Logger
)

// init is called after all the variable declarations in the package have
// evaluated their initializers, and those are evaluated only after all the
// imported packages have been initialized.
func init() {
	LogInfo = l.New(os.Stdout, "INFO: ", l.Ldate|l.Ltime|l.Lshortfile)
	LogWarning = l.New(os.Stdout, "WARNING: ", l.Ldate|l.Ltime|l.Lshortfile)
	LogError = l.New(os.Stdout, "ERROR: ", l.Ldate|l.Ltime|l.Lshortfile)
	LogDebug = l.New(os.Stdout, "DEBUG: ", l.Ldate|l.Ltime|l.Lshortfile)
}

// A TestObject represents a defined struct (structure) containing named
// fields
type TestObject struct {
	size int
	name string
	data []byte
}

// TestFunction is an example of a function doc-string and the first
// sentence should be a summary of the function starting with the
// name being declared. The function is a 'method' that extends the structure
// TestObject and takes as input a slice of float64s, a slice of ints, and
// one or more (variadic) string parameters. The method returns the sum of
// param1, the sum of parm2, and a string that combines all subsequent string
// parameters.
func (o *TestObject) TestFunction(param1 []float64, param2 []int,
	param3 ...string) (float64, int, string) {
	total1 := 0.0
	for _, v := range param1 {
		total1 += v
	}
	total2 := 0
	for _, j := range param2 {
		total2 += j
	}
	combined := ""
	for _, k := range param3 {
		combined += k
	}

	return total1, total2, combined
}

// CheckOS is a function to demonstrate the use of
// switch and if/else
func CheckOS() {
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X.")
	case "linux":
		fmt.Println("Linux.")
	default:
		fmt.Printf("%s.\n", os)
	}
	if v := "variable"; v != nil {
		fmt.Println("Not empty")
	} else {
		fmt.Println("Empty")
	}
}

// Hash is a function to demonstrate the use of
// slices, arrays, and maps
func Hash() {

}

// main would only be relevant if the package was main
func main() {
	LogInfo.Println("Starting michael...")
	LogInfo.Println("michael has done something of note.")
	LogWarning.Println("michael has done something to be concerned about.")
	LogError.Println("michael has done something erroneous.")
	LogDebug.Println("michael needs this debugging output.")
}
