Usage Notes in [nvim/notes.md](nvim/notes.md)

# Building from Source

[Instructions](https://github.com/neovim/neovim/blob/master/BUILD.md#build-prerequisites)

On Fedora:
```bash
sudo dnf -y install ninja-build cmake gcc make unzip gettext curl glibc-gconv-extra
```

Use 'Release' type as I do not care about submitting crash reports
```bash
$ git checkout stable
$ make CMAKE_BUILD_TYPE=Release
$ make CMAKE_INSTALL_PREFIX=$HOME/local/nvim install # Fails because one file is owned by root?
$ sudo chmod -R michael:michael .
$ make CMAKE_INSTALL_PREFIX=$HOME/local/nvim install
$ ln -s /home/michael/local/nvim/bin/nvim /usr/local/bin/nvim
```
